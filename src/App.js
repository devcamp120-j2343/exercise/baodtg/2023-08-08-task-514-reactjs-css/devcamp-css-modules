
import image from './assets/images/48.jpg'
import cssModule from './App.module.css';
function App() {
  return (
    <div className={cssModule.dcContainer}>
      <div className={cssModule.dcImageContainer}>
        <img src={image} alt='image user' className={cssModule.dcImage}></img>
      </div>
      <div className='dc-quote'>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      </div>
      <div className={cssModule.dcInfo}>
        <b className={cssModule.userName}>Tammy Stevent </b> &nbsp; * &nbsp;Front End Developer
      </div>
    </div>
  );
}

export default App;
